#!/usr/bin/python3

import timeit
start = timeit.default_timer()

from pandas import read_excel
import sqlite3

filename = '/home/axer/Downloads/Sample - Superstore.xls'
sqliteDBname = '/home/axer/dbTest.sqlite'
tableName = 'Orders'

def main():
	xlsFile = read_excel(filename, tableName)

	stop = timeit.default_timer()
	print('before main:', stop - start)

	columns = xlsFile.columns

	db = sqlite3.connect(sqliteDBname)
	cursor = db.cursor()


	tableHeaders = "("
	values = " VALUES("
	for column in columns:
		column = column.replace(" ", "_")
		column = column.replace("-", "")
		tableHeaders += column + ","
		values += "'{" + column + "}', "

	tableHeaders += ")"
	tableHeaders = tableHeaders.replace(",)", ")")
	values += ")"
	values = values.replace(",)", ")")
	

	query = "CREATE TABLE IF NOT EXISTS " + tableName + tableHeaders
	print(query)
	cursor.execute(query)

	# Temporary
	cursor.execute("DELETE FROM " + tableName)

	Ncolumns = len(columns)
	Nrows = len(xlsFile)

	def readFromXls(i):
		data = []
		return xlsFile.loc[i]

	query = "INSERT INTO " + tableName + tableHeaders + values

	for i in range(1, Nrows):
		record = readFromXls(i)
#		print(record)
		
#		returned = '0'
#		if record['Returned'].capitalize() == 'Yes':
#			returned = '1'
		
		for column in columns:
			print(type(column), type(record[column]))
			q = query.replace("{" + column + "}", record[column])
#		q = query.format(returned, record[columns[1]])

		#print(q)
		cursor.execute(q)
	
	db.commit()	
	db.close()
#	print('Done')


if __name__ == '__main__':
	main()

stop = timeit.default_timer()
print('this process takes:', stop - start)
